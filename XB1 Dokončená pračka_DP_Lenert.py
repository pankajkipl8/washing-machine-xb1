# NÁZEV PROGRAMU XB1 PRACÍ PROGRAM

# SOUČÁSTI
import time
import datetime as dt

#ÚVOD
()
print("<<<AUTOMATICKÁ PRAČKA XB1>>>")
time.sleep(2)

# DEFINICE ODPOČTU ČASU
def calcProcessTime(starttime, cur_iter, max_iter):

    telapsed = time.time() - starttime
    testimated = (telapsed/cur_iter)*(max_iter)

    finishtime = starttime + testimated
    finishtime = dt.datetime.fromtimestamp(finishtime).strftime("%H:%M:%S")  # in time

    lefttime = testimated-telapsed  # in seconds

    return (int(telapsed), int(lefttime), finishtime)

# VÝBĚR PRACÍHO PROGRAMU
while True:
    program = input ("VYBER SI PRACÍ PROGRAM ['1', '2', '3', '4'] A TEN NAPIŠ DO KONZOLE: ")
    
       
        
            
    if program == '1': 
        
            print ("PRACÍ PROGRAM ČÍSLO 1: Teplota: 45°c - Doba praní­: 7s.") 
            time.sleep(1)
            print ("SPOUŠTĚNÍ PROGRAMU A ODPOČÍTÁVÁČE")
            time.sleep(2)
            start = time.time()
            cur_iter = 0
            max_iter = 7

            for i in range(max_iter):
                time.sleep(1)
                cur_iter += 1
                prstime = calcProcessTime(start,cur_iter ,max_iter)
                print("PRACÍ PROGRAM BĚŽÍ­: %s(s), ZBÝVAJÍCÍ ČAS PRANÍ­: %s(s), ČAS DO KONCE­: %s"%prstime)
                if cur_iter == 7:
                    print("VYPRÁNO")
                    break

    elif program == '2':
            print ("PRACÍ PROGRAM ČÍSLO 2: Teplota: 65°c - Doba praní­: 12s.") 
            time.sleep(1)
            print ("SPOUŠTĚNÍ PROGRAMU A ODPOČÍTÁVÁČE")
            time.sleep(2)
            start = time.time()
            cur_iter = 0
            max_iter = 12

            for i in range(max_iter):
                time.sleep(1)
                cur_iter += 1
                prstime = calcProcessTime(start,cur_iter ,max_iter)
                print("PRACÍ PROGRAM BĚŽÍ­: %s(s), ZBÝVAJÍCÍ ČAS PRANÍ­: %s(s), ČAS DO KONCE­: %s"%prstime)
                if cur_iter == 12:
                    print("VYPRÁNO")
                    break

    elif program == '3':
            print("PRACÍ PROGRAM ČÍSLO 3: Teplota: 80c - Doba praní­: 25s.") 
            time.sleep(1)
            print ("SPOUŠTĚNÍ PROGRAMU A ODPOČÍTÁVÁČE")
            time.sleep(2)
            start = time.time()
            cur_iter = 0
            max_iter = 25

            for i in range(max_iter):
                time.sleep(1)
                cur_iter += 1
                prstime = calcProcessTime(start,cur_iter ,max_iter)
                print("PRACÍ PROGRAM BĚŽÍ­: %s(s), ZBÝVAJÍCÍ ČAS PRANÍ­: %s(s), ČAS DO KONCE­: %s"%prstime)   
                if cur_iter == 25:
                    print("VYPRÁNO")
                    break

    elif program == '4':
            print("PRACÍ PROGRAM ČÍSLO 4: Teplota: 92c - Doba praní­: 450s.") 
            time.sleep(1)
            print ("SPOUŠTĚNÍ PROGRAMU A ODPOČÍTÁVÁČE")
            time.sleep(2)
            start = time.time()
            cur_iter = 0
            max_iter = 450

            for i in range(max_iter):
                time.sleep(1)
                cur_iter += 1
                prstime = calcProcessTime(start,cur_iter ,max_iter)
                print("PRACÍ PROGRAM BĚŽÍ­: %s(s), ZBÝVAJÍCÍ ČAS PRANÍ­: %s(s), ČAS DO KONCE­: %s"%prstime)  
                if cur_iter == 450:
                    print("VYPRÁNO")
                    break
  
    else:
            time.sleep(2)
            print("MUSÍŠ SI VYBRAT 1 AŽ 4, JINÝ PROGRAM NENÍ DEFINOVANÝ!")
            time.sleep(3)
        
